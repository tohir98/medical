
var journalApp = angular.module('journal', []);

journalApp.controller('journalCtrl', function ($scope, $http) {
    $scope.category_id = '';
    $scope.subcategoryStatus = 'Select category first';
    $scope.group_id = '';
    $scope.subgroupStatus = 'Select group first';

    $scope.loadSubCategories = function () {
        if (!$scope.category_id) {
            return;
        }
        var url = 'get_sub_category/' + $scope.category_id ;
        $scope.subcategoryStatus = 'Loading sub categories...';
        $http.get(url)
                .success(function (data) {
                    $scope.subcategoryStatus = '';
                    $scope.subcategories = data;
                })
                .error(function () {
                    $scope.subcategoryStatus = 'Not Applicable';
                });
    };
    
     $scope.loadSubGroups = function () {
        if (!$scope.group_id) {
            return;
        }
        var url = 'get_sub_group/' + $scope.group_id ;
        $scope.subgroupStatus = 'Loading sub groups...';
        $http.get(url)
                .success(function (data) {
                    $scope.subgroupStatus = '';
                    $scope.subgroups = data;
                })
                .error(function () {
                    $scope.subgroupStatus = 'Not Applicable';
                });
    };
    
    $scope.form_submit = function(){
        $("#frm").submit();
    };



});


