var OaaStudy = OaaStudy || {};

OaaStudy.doConfirm = function (params) {
    params = params || {};
    var defaults = {
        title: 'Confirm',
        message: 'Are you sure?',
        cancelText: 'Cancel',
        acceptText: 'OK',
        onAccept: null,
        onCancel: null,
        closeOnConfirm: true,
        fade: true
    };
    var mParams = jQuery.extend({}, defaults, params);

    var html = "<div class=\"modal\" id='edConfirmModal'><div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\"><button type=\"button\" class=\"edCancelBtn close\" data-dismiss='modal' aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><h4 class=\"modal-title\"></h4></div><div class=\"modal-body\"></div><div class=\"modal-footer\"><a class=\"btn btn-default edCancelBtn\" data-dismiss='modal'></a><a class=\"btn btn-primary edAcceptBtn\" ></a></div></div></div></div>";


    $('#edConfirmModal').remove();
    $('body').append(html);

    var $confirm = $('#edConfirmModal');
    //title
    $confirm.find('.modal-header h4').html(mParams.title);
    //body
    $confirm.find('.modal-body').html(mParams.message);
    //cancel button
    $confirm.find('.btn.edCancelBtn').text(mParams.cancelText);
    //accept button
    $confirm.find('.btn.edAcceptBtn').text(mParams.acceptText);

    if (typeof mParams.onAccept === 'function') {
        $confirm.find('.edAcceptBtn')
                .click(mParams.onAccept);
    }

    if (typeof mParams.onCancel === 'function') {
        $confirm.find('.edCancelBtn').click(mParams.onCancel);
    }

    if (mParams.closeOnConfirm) {
        $confirm.find('.edAcceptBtn').attr('data-dismiss', 'modal');
    }

    if (mParams.fade) {
        $confirm.addClass('fade');
    }

    $confirm.modal();
};