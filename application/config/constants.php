<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



define('SUPPORT_EMAIL', 'help@imsuth.com');

/*
|--------------------------------------------------------------------------
| Account Types
|--------------------------------------------------------------------------
|
*/
define('ACCOUNT_TYPE_FREE_TRIAL', 1);
define('ACCOUNT_TYPE_PREMIUM', 2);

/*
|--------------------------------------------------------------------------
| User Types
|--------------------------------------------------------------------------
*/
define('USER_TYPE_SUPER_ADMIN', 1);
define('USER_TYPE_AE', 2);
define('USER_TYPE_COPD', 3);
define('USER_TYPE_GOPD', 4);

/*
|--------------------------------------------------------------------------
| User Status
|--------------------------------------------------------------------------
*/
define('USER_STATUS_ACTIVE', 1);
define('USER_STATUS_INACTIVE', 0);
define('USER_STATUS_SUSPENDED', 2);
define('USER_STATUS_TERMINATED', 3);

/*
|--------------------------------------------------------------------------
| Account Status
|--------------------------------------------------------------------------
*/
define('ACCOUNT_STATUS_ACTIVE', 1);
define('ACCOUNT_STATUS_INACTIVE', 0);

// Businnes Name
define('BUSINESS_NAME', 'IMSUTH');
define('REPLY_EMAIL', 'no_reply@imsuth.com');
define('DEFAULT_PASSWORD', 'password');

define('REG_CONFIRMATION', 'Registration Confirmation Email');

/*
  | ---------------------------------------------------
  |	Temp directory for storing files
  | ---------------------------------------------------
 */



/*
|--------------------------------------------------------------------------
| DB Tables
|--------------------------------------------------------------------------
*/
define('TBL_USERS', 'users');
define('TBL_ACCIDENT_EMERGENCY', 'accident_and_emergency_patient_card');
define('TBL_ACCIDENT_EMERGENCY_FOLDER', 'accident_and_emergency_folder');
define('TBL_CONSULTING_OUT_PATIENT_DEPARTMENT_NEW_ADULT', 'consulting_out_patient_department_new_adult');
define('TBL_CONSULTING_OUT_PATIENT_DEPARTMENT_OLD_ADULT', 'consulting_out_patient_department_old_adult');
define('TBL_CONSULTING_OUT_PATIENT_DEPARTMENT_CHILDREN_NEW', 'consulting_out_patient_department_children_new');
define('TBL_CONSULTING_OUT_PATIENT_DEPARTMENT_CHILDREN_OLD', 'consulting_out_patient_department_children_old');
define('TBL_GENERAL_OUT_PATIENT_DEPARTMENT_NEW', 'general_out_patient_department_new');
define('TBL_GENERAL_OUT_PATIENT_DEPARTMENT_OLD', 'general_out_patient_department_old');
