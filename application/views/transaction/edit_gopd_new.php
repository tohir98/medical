<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Edit General Out Patient Department (New)</h4>
    </div>
    <form role="form" method="post" action="<?= site_url('/transaction/edit_gopd_new/'. $patient_details->id ); ?>">
        <div class="modal-footer">
            <table class="table">
                <tr>
                    <td>Imsuth_No</td>
                    <td><input type="text" name="imsuth_no" class="form-control" value="<?= $patient_details->imsuth_no; ?>" /></td>
					<td>Time</td>
                    <td><input type="text" name="time" class="form-control" value="<?= $patient_details->time; ?>" /></td>
                    
                </tr>
				
                <tr>
                    <td>Surname</td>
                    <td><input type="text" name="surname" class="form-control" value="<?= $patient_details->surname; ?>" /></td>
                    <td>First Name</td>
                    <td><input type="text" name="first_name" class="form-control" value="<?= $patient_details->first_name; ?>" /></td>
                    <td>Middle Name</td>
                    <td><input type="text" name="middle_name" class="form-control" value="<?= $patient_details->middle_name; ?>" /></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><input type="text" name="address" class="form-control" value="<?= $patient_details->address; ?>" /></td>
                    <td>Tribe/Nationality</td>
                    <td><input type="text" name="tribe?nationality" class="form-control" value="<?= $patient_details->tribe/nationality; ?>" /></td>
                    <td>Age</td>
                    <td><input type="text" name="age" class="form-control" value="<?= $patient_details->age; ?>" /></td>
                </tr>
				
                <tr>
                    
                    <td>Sex</td>
                    <td><input type="text" name="sex" class="form-control" value="<?= $patient_details->sex; ?>" /></td>
					<td>Marital Status</td>
                    <td><input type="text" name="marital_status" class="form-control" value="<?= $patient_details->marital_status; ?>" /></td>
					<td>Religion</td>
                    <td><input type="text" name="religion" class="form-control" value="<?= $patient_details->religion; ?>" /></td>
                </tr>
				
				<tr>
                    
                    <td>Occupation</td>
                    <td><input type="text" name="occupation" class="form-control" value="<?= $patient_details->occupation; ?>" /></td>
					<td>Source </td>
                    <td><input type="text" name="source" class="form-control" value="<?= $patient_details->source; ?>" /></td>
					<td>Next of Kin</td>
                    <td><input type="text" name="next_of_kin" class="form-control" value="<?= $patient_details->next_of_kin; ?>" /></td>
                </tr>
				
				
				<tr>
                    
                    <td>Address (Next of Kin)</td>
                    <td><input type="text" name="address_next_of_kin" class="form-control" value="<?= $patient_details->address_next_of_kin; ?>" /></td>
					<td>Relationship </td>
                    <td><input type="text" name="relationship" class="form-control" value="<?= $patient_details->relationship; ?>" /></td>
					<td>Phone (Next of Kin)</td>
                    <td><input type="text" name="phone_next_of_kin" class="form-control" value="<?= $patient_details->phone_next_of_kin; ?>" /></td>
                </tr>
				
				<tr>
                    
                    <td>Diagnosis</td>
                    <td><input type="text" name="diagnosis" class="form-control" value="<?= $patient_details->diagnosis; ?>" /></td>
					<td>Consultant </td>
                    <td><input type="text" name="consultant" class="form-control" value="<?= $patient_details->consultant; ?>" /></td>
					<td>Receipt No</td>
                    <td><input type="text" name="receipt_no" class="form-control" value="<?= $patient_details->receipt_no; ?>" /></td>
                </tr>
				
				
				
				
				
				
                          
                <tr>

                    <td>
                        &nbsp;
                    </td>

                    <td>
                        &nbsp;
                    </td>

                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
</div>