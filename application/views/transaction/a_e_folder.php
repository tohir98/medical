<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Accident and Emergency Folder
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url($this->session->userdata('home_link'));?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Articles</a></li>-->
        <li class="active">Accident and Emergency Folder</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                    <div class="box-header">
					<h3 class="box-title">
					    <a href="<?= site_url('/transaction/add_a_e_folder') ?>" class="btn btn-primary btn-flat"> <i class="fa fa-plus-circle"></i> 
						Accident and Emergency Folder
						</a>
						</h3>
                    </div>

                <div class="box-body">
                    <?php
                    if (!empty($patients)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Sex</th>
                                    <th>Marital Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 0;
                                foreach ($patients as $patient):
                                    ?>
                               
                                    <tr>
                                        <td><?= ucfirst($patient->surname) . ' ' . ucfirst($patient->middle_name). ' ' . ucfirst($patient->last_name); ?></td>
                                        <td> <?= $patient->age ?> </td>
                                        <td> <?= $patient->sex ?> </td>
                                        <td> <?= $patient->marital_status ?> </td>

                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
												<li><a href="<?= site_url('/transaction/edit_a_e_folder/'. $patient->id)?>" onclick="return false;" class="edit">Edit</a></li>
                                                <li><a href="<?= site_url('/transaction/delete_a_e_folder/'. $patient->id)?>" onclick="return false;" class="delete">Delete</a></li>
												
                                                  <!--  <li><a href="#" onclick="return false;" class="edit">Edit</a></li>
                                                    <li><a href="<?= site_url('/transaction/delete_accident_patient/' . $patient->id) ?>" onclick="return false;" class="delete">Delete</a></li> -->
												</ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No folder has been added. <a href='/transaction/a_e_folder'>Click here to add one.</a>";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
<div class="modal" id="modal_a_e_folder">
</div>

<script>

$(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this accident patient?';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
	
	$('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_a_e_folder').modal('show');
        $('#modal_edit_a_e_folder').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_a_e_folder').html('');
            $('#modal_edit_a_e_folder').html(html);
            $('#modal_edit_a_e_folder').modal('show');
        });
        return false;
    });

</script>