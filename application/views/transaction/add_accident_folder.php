<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Add Accident Patient
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url($this->session->userdata('home_link'));?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Accident Patient Folder</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    &nbsp;
                </div>

                <div class="box-body">
                    <form method="post" action="">
                        <table class="table">
                            <tr>
                                <td>Imsuth/Folder_no</td>
                                <td><input type="text" name="imsuth_folder_no" class="form-control" /></td>
                                <td>Source</td>
                                <td><input type="text" name="source" class="form-control" /></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Surname</td>
                                <td><input type="text" name="surname" class="form-control" /></td>
                                <td>Middle Name</td>
                                <td><input type="text" name="middle_name" class="form-control" /></td>
                                <td>Last Name</td>
                                <td><input type="text" name="last_name" class="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><input type="text" name="address" class="form-control" /></td>
                                <td>Local Government</td>
                                <td><input type="text" name="local_government" class="form-control" /></td>
                                <td>Tribe</td>
                                <td><input type="text" name="tribe" class="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Age</td>
                                <td><input type="text" name="age" class="form-control" /></td>
                                <td>Sex</td>
                                <td><input type="text" name="sex" class="form-control" /></td>
                                <td>Marital Status</td>
                                <td><input type="text" name="marital_status" class="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Religion</td>
                                <td><input type="text" name="religion" class="form-control" /></td>
                                <td>Occupation</td>
                                <td><input type="text" name="occupation" class="form-control" /></td>
                                <td>Next of Kin</td>
                                <td><input type="text" name="next_of_kin" class="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Next of Kin's Address</td>
                                <td><input type="text" name="address_next_of_kin" class="form-control" /></td>
                                <td>Next of Kin's Phone</td>
                                <td><input type="text" name="phone_next_of_kin" class="form-control" /></td>
                                <td>Next of Kin's Relationship</td>
                                <td><input type="text" name="relationship_next_of_kin" class="form-control" /></td>
                            </tr>              
                            <tr>

                                <td>
                                    &nbsp;
                                </td>

                                <td>
                                    &nbsp;
                                </td>

                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
</section>
