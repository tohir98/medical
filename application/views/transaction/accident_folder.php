<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Accident and Emergency Folder
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url($this->session->userdata('home_link'));?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Accident and Emergency Folder</li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">

                        <a class="btn btn-block btn-primary pull-right" href="<?= site_url('transaction/add_folder'); ?>">
                            Add Accident and Emergency Folder
                        </a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($patient_folders)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Age</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($patient_folders as $patient_folder): ?>
                                    <tr>
                                        <td><?= ucfirst($patient_folder->id) ?></td>
                                        <td><?= ucfirst($patient_folder->surname) . ' ' . ucfirst($patient_folder->last_name) . ' ' . ucfirst($patient_folder->middle_name) ?></td>
                                        <td><?= ucfirst($patient_folder->address) ?></td>
                                        <td><?= ucfirst($patient_folder->age) ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('transaction/edit_acc_folder/' . $patient_folder->id) ?>" class="edit">Edit</a></li>
                                                    <li><a href="<?= site_url('transaction/delete_acc_folder/' . $patient_folder->id) ?>" class="delete">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No folder has been added. <a href='/transaction/add_folder'>Click here to add one.</a>";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal_edit_accident_patient_folder"></div>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this folder ?';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_accident_patient_folder').modal('show');
        $('#modal_edit_accident_patient_folder').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_accident_patient_folder').html('');
            $('#modal_edit_accident_patient_folder').html(html);
            $('#modal_edit_accident_patient_folder').modal('show');
        });
        return false;
    });
</script>

