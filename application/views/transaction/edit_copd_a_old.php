<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Edit Consulting Out Patient Department Adult (Old)</h4>
    </div>
    <form role="form" method="post" action="<?= site_url('/transaction/edit_copd_a_old/'. $patient_details->id ); ?>">
        <div class="modal-footer">
            <table class="table">
                <tr>
                    <td>Imsuth_No</td>
                    <td><input type="text" name="imsuth_no" class="form-control" value="<?= $patient_details->imsuth_no; ?>" /></td>
                    
                </tr>
				
                <tr>
                    <td>Surname</td>
                    <td><input type="text" name="surname" class="form-control" value="<?= $patient_details->surname; ?>" /></td>
                    <td>First Name</td>
                    <td><input type="text" name="first_name" class="form-control" value="<?= $patient_details->first_name; ?>" /></td>
                    <td>Middle Name</td>
                    <td><input type="text" name="middle_name" class="form-control" value="<?= $patient_details->middle_name; ?>" /></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><input type="text" name="address" class="form-control" value="<?= $patient_details->address; ?>" /></td>
                    <td>Tribe/Nationality</td>
                    <td><input type="text" name="tribe?nationality" class="form-control" value="<?= $patient_details->tribe/nationality; ?>" /></td>
                    <td>Source</td>
                    <td><input type="text" name="source" class="form-control" value="<?= $patient_details->source; ?>" /></td>
                </tr>
                <tr>
                    <td>Age</td>
                    <td><input type="text" name="age" class="form-control" value="<?= $patient_details->age; ?>" /></td>
                    <td>Sex</td>
                    <td><input type="text" name="sex" class="form-control" value="<?= $patient_details->sex; ?>" /></td>
                </tr>
                          
                <tr>

                    <td>
                        &nbsp;
                    </td>

                    <td>
                        &nbsp;
                    </td>

                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
</div>