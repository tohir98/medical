<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Edit Accident Patient Folder</h4>
    </div>
    <form role="form" method="post" action="<?= site_url('/transaction/edit_acc_folder/'. $folder_details->id ); ?>">
        <div class="modal-footer">
            <table class="table">
                <tr>
                    <td>Imsuth/Folder_no</td>
                    <td><input type="text" name="imsuth_folder_no" class="form-control" value="<?= $folder_details->imsuth_folder_no; ?>" /></td>
                    <td>Source</td>
                    <td><input type="text" name="source" class="form-control" value="<?= $folder_details->source; ?>" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Surname</td>
                    <td><input type="text" name="surname" class="form-control" value="<?= $folder_details->surname; ?>" /></td>
                    <td>Middle Name</td>
                    <td><input type="text" name="middle_name" class="form-control" value="<?= $folder_details->middle_name; ?>" /></td>
                    <td>Last Name</td>
                    <td><input type="text" name="last_name" class="form-control" value="<?= $folder_details->last_name; ?>" /></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><input type="text" name="address" class="form-control" value="<?= $folder_details->address; ?>" /></td>
                    <td>Local Government</td>
                    <td><input type="text" name="local_government" class="form-control" value="<?= $folder_details->local_government; ?>" /></td>
                    <td>Tribe</td>
                    <td><input type="text" name="tribe" class="form-control" value="<?= $folder_details->tribe; ?>" /></td>
                </tr>
                <tr>
                    <td>Age</td>
                    <td><input type="text" name="age" class="form-control" value="<?= $folder_details->age; ?>" /></td>
                    <td>Sex</td>
                    <td><input type="text" name="sex" class="form-control" value="<?= $folder_details->sex; ?>" /></td>
                    <td>Marital Status</td>
                    <td><input type="text" name="marital_status" class="form-control" value="<?= $folder_details->marital_status; ?>" /></td>
                </tr>
                <tr>
                    <td>Religion</td>
                    <td><input type="text" name="religion" class="form-control" value="<?= $folder_details->religion; ?>" /></td>
                    <td>Occupation</td>
                    <td><input type="text" name="occupation" class="form-control" value="<?= $folder_details->occupation; ?>" /></td>
                    <td>Next of Kin</td>
                    <td><input type="text" name="next_of_kin" class="form-control" value="<?= $folder_details->next_of_kin; ?>" /></td>
                </tr>
                <tr>
                    <td>Next of Kin's Address</td>
                    <td><input type="text" name="address_next_of_kin" class="form-control" value="<?= $folder_details->address_next_of_kin; ?>" /></td>
                    <td>Next of Kin's Phone</td>
                    <td><input type="text" name="phone_next_of_kin" class="form-control" value="<?= $folder_details->phone_next_of_kin; ?>" /></td>
                    <td>Next of Kin's Relationship</td>
                    <td><input type="text" name="relationship_next_of_kin" class="form-control" value="<?= $folder_details->relationship_next_of_kin; ?>" /></td>
                </tr>              
                <tr>

                    <td>
                        &nbsp;
                    </td>

                    <td>
                        &nbsp;
                    </td>

                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
</div>