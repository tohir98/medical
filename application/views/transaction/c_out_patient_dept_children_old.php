<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Consulting Out Patient Department Children (Old)
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Consulting Out Patient Department Children (Old)</li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">

                        <a class="btn btn-block btn-primary pull-right" href="<?= site_url('transaction/add_copd_c_old'); ?>">
                            Add Consulting Out Patient Department Children (Old)
                        </a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($patient)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Age</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($patient as $patient): ?>
                                    <tr>
                                        <td><?= ucfirst($patient->id) ?></td>
                                        <td><?= ucfirst($patient->surname) . ' ' . ucfirst($patient->last_name) . ' ' . ucfirst($patient->middle_name) ?></td>
                                        <td><?= ucfirst($patient->address) ?></td>
                                        <td><?= ucfirst($patient->age) ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('transaction/edit_copd_c_old/' . $patient->id) ?>" class="edit">Edit</a></li>
                                                    <li><a href="<?= site_url('transaction/delete_copd_c_old/' . $patient->id) ?>" class="delete">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No patient has been added. <a href='/transaction/add_copd_c_old'>Click here to add one.</a>";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal_edit_copd_c_old"></div>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this patient ?';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_copd_c_old').modal('show');
        $('#modal_edit_copd_c_old').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_copd_c_old').html('');
            $('#modal_edit_copd_c_old').html(html);
            $('#modal_edit_copd_c_old').modal('show');
        });
        return false;
    });
</script>

