<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Edit Accident Patients
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transactions</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    &nbsp;
                </div>

                <div class="box-body">
                    <form method="post" action="<?= site_url('/transaction/edit_accident_patient/' . $patient_details->id) ?>">
                        <table class="table">
                            <tr>
                                <td>Imsuth/Card_no</td>
                                <td><input type="text" name="imsuth" class="form-control" value="<?= $patient_details->imsuth ?>" /></td>
                                <td>Time</td>
                                <td><input type="text" name="time" class="form-control" value="<?= $patient_details->time ?>" /></td>
                                <td>Source</td>
                                <td><input type="text" name="source" class="form-control" value="<?= $patient_details->source ?>" /></td>
                            </tr>
                            <tr>
                                <td>Surname</td>
                                <td><input type="text" name="surname" class="form-control" value="<?= $patient_details->surname ?>" /></td>
                                <td>First name</td>
                                <td><input type="text" name="first_name" class="form-control" value="<?= $patient_details->first_name ?>" /></td>
                                <td>Middle name</td>
                                <td><input type="text" name="middle_name" class="form-control" value="<?= $patient_details->middle_name ?>" /></td>

                            </tr>
                            <tr>
                                <td>Address, Village, Kindred</td>
                                <td><input type="text" name="address" class="form-control" value="<?= $patient_details->address ?>" /></td>
                                <td>Local government</td>
                                <td><input type="text" name="local_government" class="form-control" value="<?= $patient_details->local_government ?>" /></td>
                                <td>Tribe</td>
                                <td><input type="text" name="tribe" class="form-control" value="<?= $patient_details->tribe ?>" /></td>
                            </tr>
                            <tr>
                                <td>Age</td>
                                <td><input type="text" name="age" class="form-control" value="<?= $patient_details->age ?>" /></td>
                                <td>Sex</td>
                                <td><input type="text" name="sex" class="form-control" value="<?= $patient_details->sex ?>" /></td>
                                <td>Marital status</td>
                                <td><input type="text" name="marital_status" class="form-control" value="<?= $patient_details->marital_status ?>" /></td>
                            </tr>
                            <tr>
                                <td>Phone </td>
                                <td><input type="text" name="phone" class="form-control" value="<?= $patient_details->phone ?>" /></td>
                                <td>Occupation</td>
                                <td><input type="text" name="occupation" class="form-control" value="<?= $patient_details->occupation ?>" /></td>
                                <td>Religion</td>
                                <td><input type="text" name="religion" class="form-control" value="<?= $patient_details->religion ?>" /></td>
                            </tr>
                            <tr>
                                <td>Next of kin</td>
                                <td><input type="text" name="next_of_kin" class="form-control" value="<?= $patient_details->next_of_kin ?>" /></td>
                                <td>Address of next of kin</td>
                                <td><input type="text" name="address_next_of_kin" class="form-control" value="<?= $patient_details->address_next_of_kin ?>" /></td>
                                <td>Relationship of next of kin</td>
                                <td><input type="text" name="relationship_next_of_kin" class="form-control" value="<?= $patient_details->relationship_next_of_kin ?>" /></td>
                            </tr>
                            <tr>
                                <td>Phone (Next of Kin)</td>
                                <td><input type="text" name="phone_next_of_kin" class="form-control" value="<?= $patient_details->phone_next_of_kin ?>" /></td>
                                <td>Accompanied By</td>
                                <td><input type="text" name="accompanied_by" class="form-control" value="<?= $patient_details->accompanied_by ?>" /></td>
                                <td>Accompanying Person's Phone</td>
                                <td><input type="text" name="accompanied_phone" class="form-control" value="<?= $patient_details->accompanied_phone ?>" /></td>
                            </tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">UPDATE</button>
                            </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>