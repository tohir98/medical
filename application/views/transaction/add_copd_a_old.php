<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Add Patient
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url($this->session->userdata('home_link'));?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transactions</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    &nbsp;
                </div>

                <div class="box-body">
                    <form method="post" action="">
                        <table class="table">
                            <tr>
                                <td>Imsuth_No</td>
                                <td><input type="text" name="imsuth" class="form-control" /></td>
								                              
                            </tr>
                            <tr>
							    <td>Surname</td>
                                <td><input type="text" name="surname" class="form-control" /></td>
                                <td>First name</td>
                                <td><input type="text" name="first_name" class="form-control" /></td>
                                <td>Middle name</td>
                                <td><input type="text" name="middle_name" class="form-control" /></td>
								
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><input type="text" name="address" class="form-control" /></td>
                                <td>Tribe/Nationality</td>
                                <td><input type="text" name="tribe_nationality" class="form-control" /></td>
								<td>Source</td>
                                <td><input type="text" name="source" class="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Age</td>
                                <td><input type="text" name="age" class="form-control" /></td>
								<td>Sex</td>
                                <td><input type="text" name="sex" class="form-control" /></td>
                            </tr>
                            
                            
                            <tr>
                                
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-primary">SUBMIT</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>