<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Change Password
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Articles</a></li>-->
        <li class="active">Users</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
               
                    <div class="box-header">
                        <h3>
                            <i class="fa fa-edit"></i>
                            <?= $user_info->first_name . ' ' . $user_info->last_name; ?>
                        </h3>
                    </div>

                <div class="box-body">
                    <form role="form" method="post">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="class_name">Current Password</label>
                                <input required type="password" class="form-control" id="cur_password" name="cur_password">
                            </div>
                            <div class="form-group">
                                <label for="class_name">New Password</label>
                                <input required type="password" class="form-control" id="new_password" name="new_password">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary" >Update</button>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>