<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Users
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Articles</a></li>-->
        <li class="active">Users</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <?php if ($this->user_auth_lib->get('access_level') == USER_TYPE_SUPER_ADMIN): ?>
                    <div class="box-header">
                        <a href="#modal-new_user" data-toggle="modal" class="btn btn-primary btn-flat"> <i class="fa fa-plus-circle"></i> New User</a>
                    </div>
                <?php endif; ?>

                <div class="box-body">
                    <?php
                    if (!empty($users)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 0;
                                foreach ($users as $user):
                                    ?>
                                    <tr>
                                        <td><?= ++$count; ?></td>
                                        <td><?= ucfirst($user->first_name) . ' ' . ucfirst($user->last_name); ?></td>
                                        <td> <?= $user->username ?> </td>
                                        <td><?= $user->status ? 'Active' : 'Inactive' ?></td>

                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#" onclick="return false;" class="edit">Edit</a></li>
                                                    <li><a href="#" onclick="return false;" class="delete">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No user has been created. <a href='#modal-new_user' data-toggle='modal'>Click here to add one.</a>";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal-new_user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New User</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="class_name">First Name</label>
                        <input required type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                    </div>
                    <div class="form-group">
                        <label for="class_name">Last Name</label>
                        <input required type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                    </div>
                    <div class="form-group">
                        <label for="class_name">Phone</label>
                        <input required type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
                    </div>
                    <div class="form-group">
                        <label for="class_name">User Type</label>
                        <select required class="form-control" name="user_type" id="user_type">
                            <option selected>Select</option>
                            <?php foreach ($user_types as $id => $val): ?>
                                <option value="<?= $id ?>"><?= $val; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="class_name">Username</label>
                        <input required type="text" class="form-control" id="username" name="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="class_name">Password</label>
                        <input required type="password" class="form-control" id="password" name="password" placeholder="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>