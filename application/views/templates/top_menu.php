<header class="main-header">
    <a href="#" class="logo"><?= $school_name ?></a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <a href="<?= $logout_url ?>" class="pull-right" style="
           padding: 14px;
           color: white;
           display: block;
           border: 1px solid blueviolet;
           background: #324292;
           ">logout</a>
    </nav>
</header>