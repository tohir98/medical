<?php

/**
 * Description of transaction_controller
 *
 * @author TOHIR

 * @property setup_model $setup_model Description
 * @property Basic_model $basic_model Description
 * @property User_auth_lib $user_auth_lib Description
 */
class transaction_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('basic_model');
    }
	
	
/** Accident and Emergency Card */
	
	public function a_e_card() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->addPatient(request_post_data())) {
                notify('success', 'Accident Patient Successfully Added');
            } else {
                notify('error', 'Unable to add Accident Patient. Please try again.');
            }
            redirect(site_url('/transaction/a_e_card'));
        }
        $data = array(
            'patients' => $this->basic_model->fetch_all_records(TBL_ACCIDENT_EMERGENCY)
        );
        $this->user_nav_lib->run_page('transaction/accident_patient', $data, 'Add Accident Patient |' . BUSINESS_NAME);
    }
	
	public function add_card() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->basic_model->addPatient(request_post_data())) {
                notify('success', 'Accident Patient Card Successfully Added');
            } else {
                notify('error', 'Unable to add Accident Patient. Please try again.');
            }
            redirect(site_url('/transaction/a_e_card'));
        }
        $data = array(
            'patients' => $this->basic_model->fetch_all_records(TBL_ACCIDENT_EMERGENCY)
        );
        $this->user_nav_lib->run_page('transaction/add_accident_patient', $data, 'Accident Patient |' . BUSINESS_NAME);
    }

	
/** Accident and Emergency Folder */	
	
	
    public function a_e_folder() {
        $this->user_auth_lib->check_login();
        $data = array(
            'patient_folders' => $this->basic_model->fetch_all_records(TBL_ACCIDENT_EMERGENCY_FOLDER)
        );

        $this->user_nav_lib->run_page('transaction/accident_folder', $data, 'Accident Patient Folder |' . BUSINESS_NAME);
    }

    public function add_folder() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->addPatientFolder(request_post_data())) {
                notify('success', 'Accident Patient Folder Successfully Added');
            } else {
                notify('error', 'Unable to add Accident Patient Folder. Please try again.');
            }
            redirect(site_url('/transaction/a_e_folder'));
        }
        $data = array();

        $this->user_nav_lib->run_page('transaction/add_accident_folder', $data, 'Accident Patient Folder |' . BUSINESS_NAME);
    }

	
/** Consulting Out Patient Department Adult New */	
	
	public function c_out_patient_dept_adult_new() {
        $this->user_auth_lib->check_login();
        $data = array(
            'patient' => $this->basic_model->fetch_all_records(TBL_CONSULTING_OUT_PATIENT_DEPARTMENT_NEW_ADULT)
        );

        $this->user_nav_lib->run_page('transaction/c_out_patient_dept_adult_new', $data, 'COPD(Adult)New |' . BUSINESS_NAME);
    }

    public function add_copd_a_new() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->addPatient(request_post_data())) {
                notify('success', 'COPD Adult New Patient Successfully Added');
            } else {
                notify('error', 'Unable to add COPD Adult New Patient. Please try again.');
            }
            redirect(site_url('/transaction/copd_a_new'));
        }
        $data = array();

        $this->user_nav_lib->run_page('transaction/add_copd_a_new', $data, 'COPD(Adult)New |' . BUSINESS_NAME);
    }
    

/** Consulting Out Patient Department Adult Old */

	

    public function c_out_patient_dept_adult_old() {
        $this->user_auth_lib->check_login();
        $data = array(
            'patient' => $this->basic_model->fetch_all_records(TBL_CONSULTING_OUT_PATIENT_DEPARTMENT_OLD_ADULT)
        );

        $this->user_nav_lib->run_page('transaction/c_out_patient_dept_adult_old', $data, 'COPD(Adult)Old |' . BUSINESS_NAME);
    }

    public function add_copd_a_old() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->addPatient(request_post_data())) {
                notify('success', 'COPD Adult Old Patient Successfully Added');
            } else {
                notify('error', 'Unable to add COPD Adult Old Patient. Please try again.');
            }
            redirect(site_url('/transaction/copd_a_old'));
        }
        $data = array();

        $this->user_nav_lib->run_page('transaction/add_copd_a_old', $data, 'COPD(Adult)Old |' . BUSINESS_NAME);
    }
	

/** Consulting Out Patient Department Children New */	

    public function c_out_patient_dept_children_new() {
        $this->user_auth_lib->check_login();
        $data = array(
            'patient' => $this->basic_model->fetch_all_records(TBL_CONSULTING_OUT_PATIENT_DEPARTMENT_CHILDREN_NEW)
        );

        $this->user_nav_lib->run_page('transaction/c_out_patient_dept_children_new', $data, 'COPD(Children)New |' . BUSINESS_NAME);
    }

    public function add_copd_c_new() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->addPatient(request_post_data())) {
                notify('success', 'COPD Children New Patient Successfully Added');
            } else {
                notify('error', 'Unable to add COPD Children New Patient. Please try again.');
            }
            redirect(site_url('/transaction/copd_c_new'));
        }
        $data = array();

        $this->user_nav_lib->run_page('transaction/add_copd_c_new', $data, 'COPD(Children)New |' . BUSINESS_NAME);
    }
	
	
/** Consulting Out Patient Department Children Old */	
	

    public function c_out_patient_dept_children_old() {
        $this->user_auth_lib->check_login();
        $data = array(
            'patient' => $this->basic_model->fetch_all_records(TBL_CONSULTING_OUT_PATIENT_DEPARTMENT_CHILDREN_OLD)
        );

        $this->user_nav_lib->run_page('transaction/c_out_patient_dept_children_old', $data, 'COPD(Children)Old |' . BUSINESS_NAME);
    }

    public function add_copd_c_old() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->addPatient(request_post_data())) {
                notify('success', 'COPD Children Old Patient Successfully Added');
            } else {
                notify('error', 'Unable to add COPD Children Old Patient. Please try again.');
            }
            redirect(site_url('/transaction/copd_c_old'));
        }
        $data = array();

        $this->user_nav_lib->run_page('transaction/add_copd_c_old', $data, 'COPD(Children)Old |' . BUSINESS_NAME);
    }

	
	
/** General Out Patient Department New */	
	
   public function g_out_patient_dept_new() {
        $this->user_auth_lib->check_login();
        $data = array(
            'patient' => $this->basic_model->fetch_all_records(TBL_GENERAL_OUT_PATIENT_DEPARTMENT_NEW)
        );

        $this->user_nav_lib->run_page('transaction/g_out_patient_dept_new', $data, 'GOPD New |' . BUSINESS_NAME);
    }

    public function add_gopd_new() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->addPatient(request_post_data())) {
                notify('success', 'GOPD New Patient Successfully Added');
            } else {
                notify('error', 'Unable to add GOPD New Patient. Please try again.');
            }
            redirect(site_url('/transaction/gopd_new'));
        }
        $data = array();

        $this->user_nav_lib->run_page('transaction/add_gopd_new', $data, 'GOPD New |' . BUSINESS_NAME);
	}

	
/** General Out Patient Department Old */	
	
	
	public function g_out_patient_dept_old() {
        $this->user_auth_lib->check_login();
        $data = array(
            'patient' => $this->basic_model->fetch_all_records(TBL_GENERAL_OUT_PATIENT_DEPARTMENT_OLD)
        );

        $this->user_nav_lib->run_page('transaction/g_out_patient_dept_old', $data, 'GOPD Old |' . BUSINESS_NAME);
    }

    public function add_gopd_old() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->basic_model->addPatient(request_post_data())) {
                notify('success', 'GOPD Old Patient Successfully Added');
            } else {
                notify('error', 'Unable to add GOPD Old Patient. Please try again.');
            }
            redirect(site_url('/transaction/gopd_old'));
        }
        $data = array();

        $this->user_nav_lib->run_page('transaction/add_gopd_old', $data, 'GOPD Old |' . BUSINESS_NAME);
	}

    public function delete_accident_patient($id) {
        if ($this->basic_model->delete(TBL_ACCIDENT_EMERGENCY, ['id' => $id])) {
            notify('success', 'Accident Patient deleted successfully');
        } else {
            notify('error', 'An error occured while deleting patient');
        }

        redirect(site_url('transaction/a_e_card'));
    }

    public function edit_accident_patient($id) {
        $this->user_auth_lib->check_login();

        $redirectUrl = site_url('transaction/a_e_card');

        if (request_is_post()) {
            if ($this->basic_model->update(TBL_ACCIDENT_EMERGENCY, request_post_data(), ['id' => $id])) {
                notify('success', 'Patient Card Updated Successfully', $redirectUrl);
            } else {
                notify('error', 'Unable to update card. Pls, try again.', $redirectUrl);
            }
        }
        $data = array(
            'patient_details' => $this->basic_model->fetch_all_records(TBL_ACCIDENT_EMERGENCY, ['id' => $id])[0],
        );
        $this->load->view('transaction/edit_accident_patients', $data);
    }

    public function delete_acc_folder($folder_id) {

        if ($this->basic_model->delete(TBL_ACCIDENT_EMERGENCY_FOLDER, ['id' => $folder_id])) {
            notify('success', 'Accident Patient Folder deleted successfully');
        } else {
            notify('error', 'An error occured while deleting folder');
        }
        redirect(site_url('/transaction/a_e_folder'));
    }

    public function edit_acc_folder($folder_id) {
        $this->user_auth_lib->check_login();
        
        if (request_is_post()) {
            if ($this->basic_model->update(TBL_ACCIDENT_EMERGENCY_FOLDER, request_post_data(), ['id' => $folder_id])) {
                notify('success', 'Folder updated successfully');
            } else {
                notify('error', 'Unable to update patient folder, pls try again');
            }

            redirect(site_url('/transaction/a_e_folder'));
        }
        
        $data = array(
            'folder_details' => $this->basic_model->fetch_all_records(TBL_ACCIDENT_EMERGENCY_FOLDER, ['id' => $folder_id])[0]
        );


        $this->user_nav_lib->run_page('transaction/edit_accident_patient_folder', $data);
    }

}
