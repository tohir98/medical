<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * 
 * @property User_nav_lib $user_nav_lib Description
 * @property CI_Loader $load Description
 * @property Basic_model $basic_model Description
 */
class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('basic_model', 'basic_model');
    }

    public function index() {
        redirect(site_url('login'));
       
    }
   

}
