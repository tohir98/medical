<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of admin_controller
 *
 * @author TOHIR
 * 
 * @property user_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 * @property user_model $u_model Description
 * @property Basic_model $basic_model Description
 * @property Agent_model $agent_model Description
 */
class Admin_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function dashboard() {
        $this->user_auth_lib->check_login();

        $data = array(
            'aec' => 1, 
            'aef' => 2, 
            'copdadnew' => 3,
            'copdadold' => 4,
			'copdchnew' => 5,
			'copdchold' => 6,
			'gopdnew' => 7,
			'gopdold' => 8,
        );
        $this->user_nav_lib->run_page('dashboard/dashboard', $data, 'Dashboard |'. BUSINESS_NAME);
    }
    
    public function dashboard_ae() {
        $this->user_auth_lib->check_login();

        $data = array(
            'aec' => 1, 
            'aef' => 2, 
		);
        $this->user_nav_lib->run_page('dashboard/dashboard_ae', $data, 'Dashboard |'. BUSINESS_NAME);
    }
    
    public function dashboard_copd() {
        $this->user_auth_lib->check_login();

        $data = array(
            'copdadnew' => 3, 
            'copdadold' => 4, 
            'copdchnew' => 5,
            'copdchold' => 6,
        );
        $this->user_nav_lib->run_page('dashboard/dashboard_copd', $data, 'Dashboard |'. BUSINESS_NAME);
    }
    
    public function dashboard_gopd() {
        $this->user_auth_lib->check_login();

        $data = array(
            'gopdnew' => 7, 
            'gopdold' => 8, 
        );
        $this->user_nav_lib->run_page('dashboard/dashboard_gopd', $data, 'Dashboard |'. BUSINESS_NAME);
    }

}
