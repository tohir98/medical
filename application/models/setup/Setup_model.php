<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of agent_model
 *
 * @author TOHIR
 */
class Setup_model extends CI_Model {

    private static $subject = "Access Granted | Agent";

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetchAgents($creator) {
        return $this->db->select('a.*, u.first_name c_first_name, u.last_name c_last_name, u2.username c_email, b.branch_name ')
                ->from(TBL_AGENTS . ' as a')
                ->join(TBL_USERS . ' as u', 'u.user_id = a.created_by')
                ->join(TBL_USERS . ' as u2', 'u2.user_id = a.user_id')
                ->join(TBL_BRANCHES . ' as b', 'b.user_id = a.created_by', 'left')
                ->where('a.created_by', $creator)
                ->get()->result();
    }

    
    public function deleteAgent($agent_id) {
        return $this->db->where('agent_id', $agent_id)
                ->delete(TBL_AGENTS);
    }

    public function getAgentById($agent_id) {
        return $this->db->get_where(TBL_AGENTS, ['agent_id' => $agent_id])->row();
    }

    public function updateAgent($id, $data) {
        return $this->db->where('agent_id', $id)
                ->update(TBL_AGENTS, array_merge($data, array(
                    'date_updated' => date('Y-m-d h:i:s')
                        )
                    )
        );
    }

    private function _sendConfirmationEmail($user_id, $params) {
        $mail_data = array(
            'header' => REG_CONFIRMATION,
            'first_name' => $params['first_name'],
            'username' => $params['email'],
            'password' => DEFAULT_PASSWORD,
            'verify_link' => site_url('verify_email/' . $user_id . '/' . $this->user_auth_lib->encrypt($params['email'] . $user_id) . '/acc'),
        );

        $msg = $this->load->view('email_templates/agent', $mail_data, true);

        return $this->mailer
                ->sendMessage(static::$subject, $msg, $params['email']);
    }

    public function get_sub_groups($group_id) {
        return $this->db->get_where('sub_groups', ['group_id' => $group_id])->result_array();
    }

    public function fetchGroups() {
        $groups = $this->db->get('groups')->result();
        if (empty($groups)) {
            return FALSE;
        }

        $result = [];

        foreach ($groups as $group) {
            $result[] = array_merge((array) $group, ['sub_group' => $this->get_sub_groups($group->group_id)]);
        }
        return $result;
    }

    public function get_sub_categories($category_id) {
        return $this->db->get_where('sub_categories', ['category_id' => $category_id])->result_array();
    }

    public function fetchCategories() {
        $categories = $this->db->get('categories')->result();
        if (empty($categories)) {
            return FALSE;
        }

        $result = [];

        foreach ($categories as $category) {
            $result[] = array_merge((array) $category, ['sub_categories' => $this->get_sub_categories($category->category_id)]);
        }

        return $result;
    }

    public function add_sub_group($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        if (!is_array($data['sub_group']) || empty($data['sub_group'])) {
            return FALSE;
        }

        $data_db = [];
        foreach ($data['sub_group'] as $sub_group) {
            $data_db[] = ['group_id' => $data['group_id'], 'sub_group' => $sub_group, 'status' => 1, 'date_created' => date('Y-m-d :i:s')];
        }
        !empty($data_db) ? $this->db->insert_batch('sub_groups', $data_db) : '';

        return TRUE;
    }
    
    public function add_sub_category($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        if (!is_array($data['sub_category']) || empty($data['sub_category'])) {
            return FALSE;
        }

        $data_db = [];
        foreach ($data['sub_category'] as $sub_category) {
            $data_db[] = ['category_id' => $data['category_id'], 'sub_category' => $sub_category, 'status' => 1, 'date_created' => date('Y-m-d :i:s')];
        }
        !empty($data_db) ? $this->db->insert_batch('sub_categories', $data_db) : '';

        return TRUE;
    }

}
