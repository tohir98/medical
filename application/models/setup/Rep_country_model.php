<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of branch_model
 *
 * @author TOHIR
 * @property User_auth_lib $user_auth_lib Description
 */
class Rep_country_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function saveCountry($data) {
        $this->db->trans_start();

        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        $c_data = array(
            'country_id' => $data['country_id'],
            'created_by' => $this->user_auth_lib->get('user_id'),
            'created_at' => date('Y-m-d h:i:s'),
            'status' => 1,
        );

        $this->db->insert(TBL_REP_COUNTRIES, $c_data);
        $rep_country_id = $this->db->insert_id();

        $c_status = [];
        if (!empty($data['status'])) {
            foreach ($data['status'] as $status) {
                $c_status[] = ['rep_country_id' => $rep_country_id, 'status_name' => $status];
            }
        }
        !empty($c_status) ? $this->db->insert_batch(TBL_REP_COUNTRY_STATUS, $c_status) : '';

        $this->db->trans_complete();

        return TRUE;
    }

    public function get_rep_countries() {
        return $this->db->select('rc.*, c.country')
                ->from(TBL_REP_COUNTRIES . ' as rc')
                ->join(TBL_COUNTRIES . ' as c', 'rc.country_id=c.country_id')
                ->get()->result();
    }

    public function get_country_statuses($rep_country_id) {
        return $this->db->get_where(TBL_REP_COUNTRY_STATUS, ['rep_country_id' => $rep_country_id])->result_array();
    }

    public function fetchRepCountries() {
        $rep_countries = $this->get_rep_countries();
        if (empty($rep_countries)) {
            return FALSE;
        }

        $result = [];

        foreach ($rep_countries as $rc) {
            $result[] = array_merge((array) $rc, ['statuses' => $this->get_country_statuses($rc->rep_country_id)]);
        }
        return $result;
    }

    public function update_rep_country_status($status, $id) {
        return $this->db->where('id', $id)
                ->update(TBL_REP_COUNTRY_STATUS, ['status' => $status]);
    }

}
