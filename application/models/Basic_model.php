<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of basic_model
 *
 * @author TOHIR
 * @property User_auth_lib $user_auth_lib Description
 */
class Basic_model extends CI_Model {

    const TBL_ARTICLE = 'articles';
    const TBL_JOURNALS = 'journals';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetch_all_records($table, $where = NULL) {

        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }

        if (is_null($where)) {
            $result = $this->db->get($table)->result();
        } else {
            $result = $this->db->get_where($table, $where)->result();
        }

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    public function fetch_journal($table, $where = []) {
        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }
        
        if (!empty($where)){
           $this->db->where($where);
        }

        return $this->db->select('t.*, c.category, g.group_name, sg.sub_group, sc.sub_category')
                ->from($table . ' as t')
                ->join('categories c', 't.category_id=c.category_id', 'left')
                ->join('sub_categories sc', 't.sub_category_id=sc.sub_category_id', 'left')
                ->join('groups g', 't.group_id=g.group_id', 'left')
                ->join('sub_groups sg', 't.sub_group_id=sg.sub_group_id', 'left')
                ->get()->result();
    }

    public function saveEntry($table, $data, $chk = FALSE) {
        if (empty($data)) {
            return FALSE;
        }

        if ($chk) {
            $data['password'] = $this->user_auth_lib->encrypt($data['password']);
            $data['created_at'] = date('Y-m-d h:i:s');
        } else {
            $data['date_created'] = date('Y-m-d h:i:s');

            $this->db->insert(self::TBL_JOURNALS, array(
                'author' => $data['author'],
                'title' => $data['title'],
                'volume' => $data['volume'],
                'issues' => $data['issues'],
                'publisher' => $data['publisher'] ? : '',
                'group_id' => $data['group_id'],
                'category_id' => $data['category_id'],
                'type' => $table,
                'url' => $data['url'],
                'year' => $data['year'],
            ));
        }


        return $this->db->insert($table, $data);
    }

    /*
     * @access public
     * @param int $id_bank
     * @param arr $data
     * @return success or fail
     */

    public function update($table, $data, $where) {
        return $this->db->where($where)
                ->update($table, $data);
    }

    /**
     * Class Delete anything
     * 
     * @access public
     * @return void
     */
    public function delete($table, $array) {
        $this->db->where($array);
        return $this->db->delete($table);
    }

    public function runSearch($search_text) {
        return $this->db
                ->select('j.*, g.group_name, c.category')
                ->from(self::TBL_JOURNALS . ' as j')
                ->join('groups g', 'g.group_id=j.group_id')
                ->join('categories c', 'c.category_id=j.category_id')
                ->like('g.group_name', $search_text)
                ->get()->result();
    }
    
    public function addPatient($data) {
        if (empty($data)) {
            return FALSE;
        }

        return $this->db->insert(TBL_ACCIDENT_EMERGENCY, array(
                'imsuth' => $data['imsuth'],
                'surname' => $data['surname'],
                'time' => $data['time'],
                'middle_name' => $data['middle_name'],
                'first_name' => $data['first_name'],
                'address' => $data['address'] ? : '',
                'local_government' => $data['local_government'],
                'tribe' => $data['tribe'],
                'age' => $data['age'],
                'sex' => $data['sex'],
                'marital_status' => $data['marital_status'],
                'religion' => $data['religion'],
                'occupation' => $data['occupation'],
                'source' => $data['source'],
                'next_of_kin' => $data['next_of_kin'],
                'tribe' => $data['tribe'],
                'phone' => $data['phone'],
                'phone_next_of_kin' => $data['phone_next_of_kin'],
                'accompanied_by' => $data['accompanied_by'],
                'accompanied_phone' => $data['accompanied_phone'],
                'address_next_of_kin' => $data['address_next_of_kin'],
                'relationship_next_of_kin' => $data['relationship_next_of_kin']
            ));
        
    }
    
    public function addPatientFolder($data) {
        if (empty($data)) {
            return FALSE;
        }

        return $this->db->insert(TBL_ACCIDENT_EMERGENCY_FOLDER, array(
                'imsuth_folder_no' => $data['imsuth_folder_no'],
                'surname' => $data['surname'],
                'middle_name' => $data['middle_name'],
                'last_name' => $data['last_name'],
                'address' => $data['address'] ? : '',
                'local_government' => $data['local_government'],
                'tribe' => $data['tribe'],
                'age' => $data['age'],
                'sex' => $data['sex'],
                'marital_status' => $data['marital_status'],
                'religion' => $data['religion'],
                'occupation' => $data['occupation'],
                'source' => $data['source'],
                'next_of_kin' => $data['next_of_kin'],
                'address_next_of_kin' => $data['address_next_of_kin'],
                'relationship_next_of_kin' => $data['relationship_next_of_kin'],
                'phone_next_of_kin' => $data['phone_next_of_kin']
            ));
    }

}
