<?php

/**
 * Description of DBMigration
 *
 * @author JosephT
 */
class DBMigration {

    /**
     *
     * @var DBV_Adapter_Interface
     */
    protected $_adapter;
    protected $revisionsDir;
    protected $schemaTableName = 'dvbc__schema_version';

    /**
     *
     * @var DBMigrationRevision[]
     */
    protected $newRevisions = [];

    /**
     *
     * @var DBMigrationRevision[]
     */
    protected $appliedRevisions = [];

    protected $maxRevision = 0;
    protected $latestRevision = 0;
    protected $dbConfig = array();
    protected $badRevisions = array();
    protected $dbGroup;

    public function __construct($revisonsDir = null, $dbGroup = 'default', array $dbConfig = array()) {
        if ($revisonsDir) {
            $this->revisionsDir = realpath($revisonsDir);
        } else {
            $this->revisionsDir = realpath(__DIR__ . '/revisons');
        }

        if (!file_exists($this->revisionsDir) || !is_dir($this->revisionsDir) || !is_readable($this->revisionsDir)) {
            //init revisions directory 
            throw new DBMigrationException("Either the specified revisions directory ({$this->revisionsDir}) does not exist or is not a readable directory");
        }

        $this->dbGroup = $dbGroup;

        $this->dbConfig = $dbConfig;
        $badRevFile = __DIR__ . '/bad_revisions_' . $dbGroup . '.txt';

        if (file_exists($badRevFile)) {
            $this->badRevisions = file($badRevFile);
            array_walk($this->badRevisions, function(&$rev) {
                $rev = intval(trim($rev));
            });
            $this->badRevisions = array_filter($this->badRevisions);
        }
    }

    public function getDbConfig() {
        return $this->dbConfig;
    }

    public function setDbConfig($dbConfig) {
        $this->dbConfig = $dbConfig;
        return $this;
    }

    public function runMigration($mode = DBMigrationMode::NON_INTERACTIVE, $startVersion = 0) {
        //load revisions from directory...
        self::el('Loading applied revisions...');
        $this->loadAppliedRevisions();
        self::el('Total revisions loaded: ', count($this->appliedRevisions));
        self::el('DB currently at version : ', $this->maxRevision, PHP_EOL);
        self::el('Loading new revisions from filesystem');
        if ($this->maxRevision < $startVersion) {
            $this->maxRevision = $startVersion;
            self::el('Migration will be started at version : ', $this->maxRevision, PHP_EOL);
        }
        $this->loadNewRevisions();
        
        self::el('Loaded', count($this->newRevisions), 'revision(s)');
        self::el('Target Schema Version : ', $this->latestRevision);

        switch ($mode) {
            case DBMigrationMode::INTERACTIVE:
                $this->_runInteractive();
                break;

            case DBMigrationMode::NON_INTERACTIVE:
                $this->_runNonInteractive();
                break;

            case DBMigrationMode::DRY_RUN:
            default :
                $this->_runDryRun();
                break;
        }
    }

    private function _runInteractive() {
        self::el("Running in interactive mode.");
        echo 'The following revisions will be applied: ', PHP_EOL;
        foreach ($this->newRevisions as $revision) {
            echo $revision->getVersionId(), ': ', $revision->getTitle(), PHP_EOL;
        }

        $resp = self::readInput('Do you want to apply these revisions (no)? [yes/no]', 'n');
        if (strtolower(trim($resp)) === 'yes') {
            $this->_runNonInteractive();
        } else {
            DBMigration::el('OK, Bye!');
        }
    }

    private function _runNonInteractive() {
        self::el('Starting migrations...');

        $adapter = $this->_getAdapter();
        $lastVersion = $this->maxRevision;
        foreach ($this->newRevisions as $revision) {
            if (!in_array($revision->getVersionId(), $this->badRevisions)) {
                self::el('Migrating from ', $lastVersion, 'to', $revision->getVersionId(), ': ', $revision->getTitle());
                $queries = $this->getContent($revision);
                $adapter->query($queries);
                self::el('Successfully applied');
                $this->_saveAppliedRevision($revision);
            } else {
                self::el('Skipped Bad Revision, ', $revision->getVersionId());
            }

            $lastVersion = $revision->getVersionId();
        }
        self::el('Successfully migrated DB to version : ', $lastVersion);
    }

    private function _runDryRun() {
        self::el(str_repeat('+', 30));
        self::el('Running in Dry Run mode, pipe into less for better result');
        self::el(str_repeat('+', 30));
        self::el("DB will be migrated from version {$this->maxRevision} to {$this->latestRevision}");
        foreach ($this->newRevisions as $aRevision) {
            self::el('Revision Version: ', $aRevision->getVersionId());
            self::el('Description: ', $aRevision->getTitle());
            self::el('File: ', $aRevision->getFile());
            self::el(str_repeat('-', 30));
            self::el('Content', PHP_EOL, "\t", $this->getContent($aRevision));
        }
    }

    private function _saveAppliedRevision(DBMigrationRevision $revision) {
        $sql = $this->_buildInsert([$revision->toFieldArray()]);
        return $this->_getAdapter()->query($sql);
    }

    private function checkTableExists() {
        $db = $this->_getAdapter();
        try {
            $db->query("DESCRIBE  `{$this->schemaTableName}`");
        } catch (Exception $ex) {
            self::el('Seems SCHEM DB is not existent,', $ex->getMessage());
            $createQuery = <<<SQL
CREATE TABLE IF NOT EXISTS `{$this->schemaTableName}`(
  `version_id` INT NOT NULL,
  `file` VARCHAR(255) NOT NULL,
  `checksum` VARCHAR(42) NOT NULL,
  `title` VARCHAR(255),
  `created_on` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version_id`)
);
SQL;
            $db->query($createQuery);
        }
    }

    private function loadAppliedRevisions($isRecall = false) {
        //check if table exists, if not create
        $this->checkTableExists();
        //load revisions applied
        $result = $this->_getAdapter()->query("SELECT * FROM `{$this->schemaTableName}`");
        /* @var $result PDOStatement */
        if ($result->rowCount() < 1 && !$isRecall) {
            //Legacy, no result found load from CSV
            $migrationStore = __DIR__ . '/revision_' . $this->dbGroup . '.csv';
            $storeHandle = fopen($migrationStore, 'r+');
            if (!$storeHandle || !is_resource($storeHandle)) {
                self::el('Legacy Migration Store was not found.');
                return;
            }

            $rowsFromCsv = array();
            while (($row = fgetcsv($storeHandle)) !== FALSE) {
                $rowsFromCsv[] = $row;
            }
            fclose($storeHandle);

            if (count($rowsFromCsv) < 1) {
                self::el('The CSV file does not contain any revision file.');
                return;
            }
            //insert
            self::el('Found revisions in CSV: ', count($rowsFromCsv));
            self::el('Saving revisions to database.');
            $sql = $this->_buildInsert($rowsFromCsv);
            $this->_getAdapter()->query($sql);
            self::el('Completed saving of data from LEGACY STORE');
            //then recall
            self::el('Recalling the applied revisions');
            $result->closeCursor();
            $this->loadAppliedRevisions(true);
        }

        //since this uses the same column ordering as legacy
        $result->setFetchMode(PDO::FETCH_NUM);

        foreach ($result as $row) {
            $revision = DBMigrationRevision::fromLine($row);
            $path = $this->revisionsDir . '/' . $revision->getFile();

            if (!file_exists($path)) {
                throw new DBMigrationException("Applied migration at version {$revision->getVersionId()}, {$revision->getFile()} was not found");
            } else {
                $fileChecksum = DBMigrationRevision::computeChecksum($path);
                if ($fileChecksum !== $revision->getChecksum()) {
                    throw new DBMigrationException("Applied migration is no longer valid, applied migration at Version {$revision->getVersionId()} = {$revision->getChecksum()} ; Revision File = {$fileChecksum}");
                }
            }
            $this->appliedRevisions[$revision->getVersionId()] = $revision;
        }

        //close cursor
        $result->closeCursor();

        if (!empty($this->appliedRevisions)) {
            krsort($this->appliedRevisions, SORT_NUMERIC);
            $this->latestRevision = $this->maxRevision = current($this->appliedRevisions)->getVersionId();
        }

        return $this;
    }

    private function _buildInsert($rows) {
        $q = "INSERT INTO `{$this->schemaTableName}` (version_id, file, checksum, title, created_on) VALUES ";

        $values = array();
        foreach ($rows as $row) {
            $rowEscd = [];
            foreach ($row as $value) {
                $rowEscd[] = sprintf("'%s'", addslashes($value));
            }
            $values[] = '(' . join(', ', $rowEscd) . ')';
        }

        return $q . join(', ', $values);
    }

    protected function loadNewRevisions() {
        $files = glob($this->revisionsDir . DIRECTORY_SEPARATOR . '*.sql');
        foreach ($files as $aFile) {
            $aRevision = DBMigrationRevision::fromFile($aFile);
            if ($aRevision) {
                $version = $aRevision->getVersionId();
                //check if revision exists 
                if (array_key_exists($version, $this->appliedRevisions)) {
                    //checks...
                    $storedRevision = $this->appliedRevisions[$version];
                    if (!$storedRevision->equals($aRevision)) {
                        throw new DBMigrationException("Applied migration is no longer valid, applied migration at Version {$version} = {$storedRevision} ; Revision File = {$aRevision}");
                    }
                } else {
                    if ($aRevision->getVersionId() > $this->maxRevision) {
                        //check if version has been loaded, throw exception on duplicate
                        if (array_key_exists($version, $this->newRevisions)) {
                            $existing = $this->newRevisions[$version];
                            throw new DBMigrationException("Duplicate found at version: {$version}, between {$existing} and {$aRevision}");
                        }

                        $this->newRevisions[$version] = $aRevision;
                    }
                }
            }
        }

        if (!empty($this->newRevisions)) {
            ksort($this->newRevisions, SORT_NUMERIC);
            $firstNewVersion = current($this->newRevisions)->getVersionId();
            if (($firstNewVersion - $this->maxRevision) > 1) {
                self::el('WARNING!!!', 'Versions might be missing', $this->maxRevision, ' Jumps to ', $firstNewVersion);
            }
            $this->latestRevision = end($this->newRevisions)->getVersionId();
        }
    }

    /**
     * @return DBV_Adapter_Interface
     * @throws DBMigrationException
     */
    protected function _getAdapter() {
        if (!$this->_adapter) {
            $file = 'adapters' . DS . strtolower(DB_ADAPTER) . '.php';
            if (file_exists($file)) {
                require_once $file;

                $class = 'DBV_Adapter_' . DB_ADAPTER;
                if (class_exists($class)) {
                    $adapter = new $class;
                    /* @var $adapter DBV_Adapter_Interface */
                    $adapter->connect($this->dbConfig['host'], $this->dbConfig['port'], $this->dbConfig['username'], $this->dbConfig['password'], $this->dbConfig['database']);
                    $this->_adapter = $adapter;
                }
            } else {
                throw new DBMigrationException("Adapter file {$file} was not found");
            }
        }

        return $this->_adapter;
    }

    public function getContent(DBMigrationRevision $revision) {
        $file = $this->revisionsDir . DS . $revision->getFile();
        if (!file_exists($file) || !is_readable($file) || ($content = file_get_contents($file)) === FALSE) {
            throw new DBMigrationException("Revision file [{$file}] for version {$revision->getVersionId()} could not be read!");
        }

        return $content;
    }

    /**
     * Echoes to screen an outpu value.
     * 
     */
    public static function el() {
        $op = [];
        $op[] = date('[Y-m-d H:i:s]');
        $op[] = "\t";
        foreach (func_get_args() as $arg) {
            if (is_scalar($arg)) {
                $op[] = $arg;
            } else if (is_object($arg) && is_a($arg, '\Exception')) {
                /* @var $arg Exception */
                $op[] = 'Exception: ' . get_class($arg) . ":\n\t";
                $op[] = $arg->getMessage() . "\n\t";
                $op[] = $arg->getTraceAsString();
            } else {
                $op[] = print_r($arg, true);
            }
        }

        echo join(" ", $op), PHP_EOL;
    }

    public static function readInput($str, $default = "") {
        echo $str . " ";
        $fh = fopen("php://stdin", "r");
        $response = trim(fgets($fh));
        fclose($fh);
        return strlen($response) ? $response : $default;
    }

}

class DBMigrationRevision {

    protected $id;
    protected $title;
    protected $file;
    protected $checksum;

    public function __construct($id, $file, $checksum, $title = null) {
        $this->id = intval($id);
        $this->title = $title;
        $this->file = $file;
        $this->checksum = $checksum;
    }

    public function getVersionId() {
        return $this->id;
    }

    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getFile() {
        return $this->file;
    }

    public function getChecksum() {
        return $this->checksum;
    }

    public function equals(DBMigrationRevision $revision) {
        return $this->file === $revision->file && $this->checksum === $revision->checksum;
    }

    public function __toString() {
        return '[' . $this->file . ':md5:' . $this->checksum . ']';
    }

    public function toFieldArray() {
        return [$this->id, $this->file, $this->checksum, $this->title, date('Y-m-d H:i:s')];
    }

    /**
     * 
     * @param array $line
     * @return \DBMigrationRevision
     */
    public static function fromLine($line) {
        //$id, $file, $checksum, $file = null
        return new DBMigrationRevision($line[0], $line[1], $line[2], $line[3]);
    }

    /**
     * Creates revision object from a file name.
     * @param type $file
     * @return null|\DBMigrationRevision
     */
    public static function fromFile($file) {
        $baseName = basename($file);
        $matches = array();
        if (!preg_match('/^(\d+)_(.+)\.sql$/i', $baseName, $matches)) {
            DBMigration::el('skipping file', $file, 'it doesn\'t match pattern');
            return null;
        }

        $version = $matches[1];
        $description = preg_replace('/[^a-zA-Z0-9]+/', ' ', $matches[2]);

        return new DBMigrationRevision($version, $baseName, self::computeChecksum($file), $description);
    }

    public static function computeChecksum($file) {
        return md5_file($file);
    }

}

class DBMigrationMode {

    const INTERACTIVE = 'interactive';
    const NON_INTERACTIVE = 'non_interactive';
    const DRY_RUN = 'dryrun';

}

class DBMigrationException extends Exception {
    
}
