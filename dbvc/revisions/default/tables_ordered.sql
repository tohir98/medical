-- ----------------------------
-- Table structure for `modules`
-- ----------------------------
CREATE TABLE IF NOT EXISTS `modules` (
  `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `requires_login` int(10) unsigned DEFAULT NULL,
  `menu_order` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES ('1', 'Administration', 'administration', '1', '1', '100');
INSERT INTO `modules` VALUES ('2', 'Records', 'transaction', '1', '1', '100');
INSERT INTO `modules` VALUES ('3', 'Reports/Analytics', 'report', '1', '1', '200');



CREATE TABLE IF NOT EXISTS `module_perms` (
  `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `menu_order` smallint(6) DEFAULT '100',
  PRIMARY KEY (`perm_id`),
  KEY `FK_module_perms_module_id` (`module_id`),
  CONSTRAINT `module_perms_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of module_perms
-- ----------------------------
INSERT INTO `module_perms` VALUES ('1', '1', 'Users', 'users', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('2', '1', 'User Logs', 'user_logs', '1', '1', '200');
INSERT INTO `module_perms` VALUES ('3', '2', 'Accident Patient', 'accident_patient', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('4', '2', 'Consulting Out Patient', 'c_out_patient', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('5', '2', 'General Out Patient', 'g_out_patient', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('6', '3', 'Income Report', 'income_report', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('7', '3', 'Asset Report', 'asset_report', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('11', '3', 'All Reports', 'all_report', '1', '1', '100');


-- ----------------------------
-- Table structure for `users`
-- ----------------------------
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `user_type` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('4', 'admin@aol.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2015-10-05 11:02:26', null, '1', '1', 'Super', 'Admin', null);


-- ----------------------------
-- Table structure for `user_perms`
-- ----------------------------
CREATE TABLE IF NOT EXISTS `user_perms` (
  `user_id` int(255) unsigned NOT NULL,
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_perms
-- ----------------------------
INSERT INTO `user_perms` VALUES ('4', '1', '1');
INSERT INTO `user_perms` VALUES ('4', '2', '1');
INSERT INTO `user_perms` VALUES ('4', '3', '2');
INSERT INTO `user_perms` VALUES ('4', '4', '2');
INSERT INTO `user_perms` VALUES ('4', '5', '3');
INSERT INTO `user_perms` VALUES ('4', '6', '3');
INSERT INTO `user_perms` VALUES ('4', '7', '3');
INSERT INTO `user_perms` VALUES ('4', '8', '4');
INSERT INTO `user_perms` VALUES ('4', '9', '4');
INSERT INTO `user_perms` VALUES ('4', '10', '4');
INSERT INTO `user_perms` VALUES ('4', '11', '3');
