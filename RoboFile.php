<?php
use packager\PackagerTasks;
use Robo\Result;
use Robo\Task\SshExecTask;
use Robo\Tasks;

 //crappy codeIgniter check
if (!defined('BASEPATH')) {
    define('BASEPATH', __DIR__ . '/system');
}

//crappy codeIgniter check
if (!defined('APP_ROOT')) {
    define('APP_ROOT', __DIR__ . DIRECTORY_SEPARATOR);
}

define('ENVIRONMENT', getenv('TB_ENV') ? : 'development');
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'development':
            ini_set('display_errors', 'On');
            break;
        case 'testing':
        case 'production':
            ini_set('display_errors', 'Off');
            break;

        default:
            exit('The application environment is not set correctly.');
    }
}
 
 	 define('FCPATH', __DIR__);
	require_once 'dbvc/DBMigration.php';
	require_once 'application/config/constants.php';

class RoboFile extends \Robo\Tasks
{
    // define public methods as commands
	
	/**
     * Perform database migration, see wiki at https://bitbucket.org/talentbase/talentbasev01/wiki/Database%20Migration%20Guide
     * for details on database migration
     * @param string $dbGroup the group to migrate.
     * @option $start int the start version
     * @option $mode the mode in which to run the migration
     *                  Available modes include interactive|non_interactive|dryrun
     * @return int 0 on success any other value otherwise
     */
	 


    public function dbMigrate($dbGroup = 'default', $opts = ['start' => 1, 'mode' => DBMigrationMode::NON_INTERACTIVE]) {
        $mode = $opts['mode'];
        $startVersion = $opts['start'];

        return include('dbvc/migration.php');
    }
}